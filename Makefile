FINAL_NAME := mathphys
TEX_COMPILER := pdflatex -interaction=nonstopmode
NPROCS := $(shell grep -c 'processor' /proc/cpuinfo)

WAIT_MARK := \033[1;33m[ ]\033[0m
FAIL_MARK := \033[1;31m[x]\033[0m
SUCCESS_MARK := \033[1;32m[\342\234\224]\033[0m

TMP_FILES_KEYS := -name "*.out" -delete -o -name "*.toc" -delete -o -name "*.aux" -delete
TMP_FILES_KEYS += -o -name "*.log" -delete -o -name "*.synctex.gz" -delete -o -name "*backup" -delete
TMP_FILES_KEYS += -o -name "*~" -delete -o -name "*.bak" -delete -o -not -path './$(FINAL_NAME).pdf' -name "*.pdf" -delete

GS_OPTIMIZER := gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6
GS_OPTIMIZER += -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -dPrinted=false

SRC_IMAGES := $(wildcard ./*/images/*.tex)
PDF_IMAGES := $(patsubst %.tex, %.pdf, $(SRC_IMAGES))

build:
	printf "\033[1mСompilation images, using $(NPROCS) threads...\033[0m\nLogs will be in \033[2m<filename>.mk.log\033[0m file.\n"
	make --silent --jobs=$(NPROCS) images
	
	printf "\033[1mСompilation main pdf file...\033[0m\n"
	(cd ./main && $(TEX_COMPILER) main.tex>>main.mk1.log)
	printf "$(SUCCESS_MARK) 1st compilation is completed, logs in the \033[2mmain/main.mk1.log\033[0m file.\n"
	(cd ./main && $(TEX_COMPILER) main.tex>>main.mk2.log)
	printf "$(SUCCESS_MARK) 2nd compilation is completed, logs in the \033[2mmain/main.mk2.log\033[0m file.\n"
	
	if [ "$(GS)" != "no" ]; then \
		printf "$(WAIT_MARK) Optimize main pdf file using Ghostscript...";\
		$(GS_OPTIMIZER) -sOutputFile=$(FINAL_NAME).pdf ./main/main.pdf;\
		printf "\033[1K\r$(SUCCESS_MARK) Main pdf file is optimized by Ghostscript.\n";\
	else \
		cp ./main/main.pdf $(FINAL_NAME).pdf;\
		printf "$(SUCCESS_MARK) The result is copied to the current root directory.\n";\
    fi
	printf "$(SUCCESS_MARK) Finished! You can run 'make view' for viewing result (\033[2m$(FINAL_NAME).pdf\033[0m).\n"


images: $(PDF_IMAGES)


%.pdf: %.tex
	if $(TEX_COMPILER) -output-directory $(dir $<) $<>>$(basename $<).mk.log; then \
		printf "$(SUCCESS_MARK) Image \033[2m$<\033[0m is compiled.\n"; \
	else \
		printf "$(FAIL_MARK) Image \033[2m$<\033[0m is failed, logs in the \033[2m$(basename $<).mk.log\033[0m file.\n"; \
    fi


view:
	xdg-open $(FINAL_NAME).pdf 2>/dev/null || echo "Failed. Can't open $(FINAL_NAME).pdf."


clean:
	find . -type f $(TMP_FILES_KEYS) 
	printf "$(SUCCESS_MARK) Cleaned.\n"


.PHONY: build clean images view
.SILENT:
