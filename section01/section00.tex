\documentclass[../main/main]{subfiles}
\begin{document}
\section{Введение}
\subsection{Классификация ЛДУ в частных производных 2-го порядка}
Пусть $u: \Omega \rightarrow \mathbb{R}$, $\Omega \in \mathbb{R}^n$, $u \in C^2(\Omega)$.
Введем обозначение, которое будем использовать на~протяжении всего курса
$$
\pdv{u}{x_i} = u_{x_i}.
$$
Рассмотрим дифференциальное уравнение второго порядка в~частных производных, записанное через оператор~$L$
\begin{equation}\label{eq1:lde2partial}
(L u)(x) = \sum\limits_{i,\,j=1}^{n} a_{ij}(x) u_{x_i x_j} + \sum\limits_{k=1}^{n} b_{k}(x) u_{x_k} + c(x) u = f(x).
\end{equation}
Считаем, что~$a$, $b$, $c$, $f$ гладкие.
Матрица $A = (a_{ij})_{i,j=1}^{n}$.
Так~как $u \in C^2(\Omega)$, то~$u_{x_i x_j} = u_{x_j x_i}$, и~тогда можно сделать матрицу~$A$ симметричной:
$\widetilde{a}_{ij} = \widetilde{a}_{ji} = \frac{a_{ij} + a_{ji}}{2}$.
В~дальнейшем будем считать матрицу~$A$ симметричной, и~раз она симметричная, то~все ее~собственные числа вещественные.
В~зависимости от~собственных чисел матрицы выделяют три типа (в~конкретной точке или~области).
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\Roman*.]
    \item Все собственные числа одного знака~--- \textbf{эллиптический тип}.
    \item Одно собственное число имеет отличный от остальных знак~--- \textbf{гиперболический тип}.
    \item Одно собственное число равно нулю, остальные одинакового знака~--- \textbf{параболический тип}.
\end{enumerate}
Если~$n \geqslant 3$, то~данная классификация не~является полной.
Рассмотрим несколько примеров.
\begin{enumerate}[noitemsep,leftmargin=*,label=\arabic*)]
\item Оператор Лапласа: $\Delta u = u_{x_1 x_1} + ... + u_{x_n x_n}$.
    Матрица $A$ единичная, тип эллиптический.
\item Уравнение струны: $u(t,x)$, $u_{tt} - a^2 u_{xx} = f$,
    матрица $A = \left(\begin{smallmatrix}1 & 0 \\ 0 & -a^2 \end{smallmatrix} \right)$, гиперболический тип.
\item Уравнение теплопроводности $u(t,x_1,x_2,x_3)$, $u_t - a^2 \Delta u = f$\footnote{в~данном уравнение оператор Лапласа берется только по~координатам},
    матрица $A = \left(\begin{smallmatrix} 0 & 0 & 0 & 0 \\ 0 & -a^2 & 0 & 0 \\ 0 & 0 & -a^2 & 0 \\ 0 & 0 & 0 & -a^2 \\ \end{smallmatrix} \right)$, параболический тип.
\end{enumerate}
Рассмотрим замену переменных: $\Omega \rightarrow \widetilde{\Omega}$, $x \rightarrow y=y(x)$,
$y \in C^2$ и~существует обратное преобразование.
$\widetilde{u}$~--- $u$ с~новыми переменными.
$$
J = \begin{bmatrix} 
    \pdv{y_1}{x_1} & \pdv{y_1}{x_2} & \dots  & \pdv{y_1}{x_n} \\
    \pdv{y_2}{x_1} &         \ddots &        & \vdots        \\
    \vdots         &                & \ddots & \vdots        \\
    \pdv{y_n}{x_1} &          \dots & \dots  & \pdv{y_n}{x_n} \\
\end{bmatrix},
\quad \det J \neq 0.
$$
\begin{equation*}
\begin{alignedat}{1}
    & u_{x_i}     = \pdv{}{x_i} (\widetilde{u}(y(x))) = \sum\limits_{k=1}^{n} \widetilde{u}_{y_k} \pdv{y_k}{x_i} \\
    & u_{x_i x_j} = \sum\limits_{k,\, l=1}^{n} \widetilde{u}_{y_k y_l} \pdv{y_k}{x_i} \pdv{y_l}{x_j} + \underbrace{\dots}_{\text{младшие члены}} \\
\end{alignedat}
\end{equation*}
Так~как тип определяется только матрицей~$A$,
то~есть коэффициентами перед старшими членами, младшие члены рассматривать не~будем.
$$
\sum\limits_{i,\,j=1}^{n} a_{ij}(x) u_{x_i x_j}
\quad \rightarrow \quad
\sum\limits_{i,\,j=1}^{n} a_{ij}(x(y)) \sum\limits_{k,\,l=1}^{n} \widetilde{u}_{y_k y_l} \pdv{y_k}{x_i} \pdv{y_l}{x_j} = 
\sum\limits_{k,\,l=1}^{n} \widetilde{a}_{kl} \widetilde{u}_{y_k y_l},
$$
где
$$
\widetilde{a}_{kl} = \sum\limits_{i,\,j=1}^{n} a_{ij}  \pdv{y_k}{x_i} \pdv{y_l}{x_j} = (A \nabla y_k, \nabla y_l)
$$
новые коэффициенты при~старших производных.
Рассмотрим следующее выражение, где~$\xi$~--- некоторый вектор,
$$
(\widetilde{A} \, \xi, \xi) =
\sum\limits_{k,\,l=1}^{n} \widetilde{a}_{kl} \xi_k \xi_l = 
\sum\limits_{k,\,l=1}^{n} \left( \sum\limits_{i,\,j=1}^{n} a_{ij} \pdv{y_k}{x_i} \pdv{y_l}{x_j} \right) \xi_k \xi_l = 
\sum\limits_{i,\,j=1}^{n} a_{ij}
    \underbrace{\left( \sum\limits_{l=1}^{n} \pdv{y_l}{x_j} \xi_l \right)}_{\widetilde{\xi}_j}
    \underbrace{\left( \sum\limits_{k=1}^{n} \pdv{y_k}{x_i} \xi_k \right)}_{\widetilde{\xi}_i}.
$$
Заметим, что $\widetilde{\xi} = J^T \xi$.
Тогда выражение выше запишется как
$$
(A J^T \xi, J^T \xi) = (A J^T \xi)^T J^T \xi = \xi^T J A^T J^T \xi = (J A J^T \xi)^T \xi = (J A J^T \, \xi, \xi),
$$
то~есть $\widetilde{A} = J A J^T$. Закон сохранения квадратичных форм.
Сохраняет число положительных и~отрицательных собственных чисел.
Таким образом тип уравнения не~меняется при~невырожденной замене независимой переменной.

\subsubsection*{Уравнение характеристик}
Рассмотрим некоторую гладкую функцию $\omega(x_1,...,x_n)$, действующую в~$\mathbb{R}$.
Составим уравнение
\begin{equation}\label{eq1:characteristics}
(A \nabla \omega, \nabla \omega) = 0
\quad \text{или, записав подробнее} \quad
\sum\limits_{i,\,j=1}^{n} a_{ij} \pdv{\omega}{x_i} \pdv{\omega}{x_j} = 0.
\end{equation}
Это уравнение называется уравнением характеристик для уравнения~(\ref{eq1:lde2partial}).
Если~$\omega(x)$~--- решение~(\ref{eq1:characteristics}),
то~поверхность $\omega(x) = \text{const}$ называется характеристической поверхностью.

Покажем, что характеристики инвариантны относительно невырожденной замены переменных.
Пусть $\omega(x_1,...,x_n)$~--- решение~(\ref{eq1:characteristics}), а~$\widetilde{\omega}(y_1,...,y_n)$~--- решение уравнения
$$
\sum\limits_{k,\,l=1}^{n} \widetilde{a}_{kl} \pdv{\widetilde{\omega}}{y_k} \pdv{\widetilde{\omega}}{y_l} = 0.
$$
Замена переменных такая~же, как и~та, что рассматривали выше.
Поэтому
$$
\pdv{\omega}{x_i} = \sum\limits_{l=1}^{n} \pdv{\widetilde{\omega}}{y_l} \pdv{y_l}{x_i}, \qquad
\pdv{\omega}{x_j} = \sum\limits_{k=1}^{n} \pdv{\widetilde{\omega}}{y_k} \pdv{y_k}{x_j}.
$$
Тогда
$$
\sum\limits_{i,\,j=1}^{n} a_{ij} \pdv{\omega}{x_i} \pdv{\omega}{x_j} =
\sum\limits_{i,\,j=1}^{n} a_{ij} \sum\limits_{l=1}^{n} \pdv{\widetilde{\omega}}{y_l} \pdv{y_l}{x_i} \sum\limits_{k=1}^{n} \pdv{\widetilde{\omega}}{y_k} \pdv{y_k}{x_j} = 
\sum\limits_{k,\,l=1}^{n} \widetilde{a}_{kl} \pdv{\widetilde{\omega}}{y_k} \pdv{\widetilde{\omega}}{y_l}.
$$

Вернемся к~началу и~рассмотрим один простой пример. Пусть $n = 2$ и $x_1 = x$, $x_2 = y$.
Запишем уравнение~(\ref{eq1:lde2partial}) в~виде
\begin{equation}\label{eq1:lde2partial-2}
A u_{xx} + 2 B u_{xy} + C u_{yy} + R u_x + S u_y + K u = f.
\end{equation}
Определим его тип.
$$
\left|
\begin{matrix}
    A - \lambda & B \\
    B & C - \lambda \\
\end{matrix}
\right|
= \lambda^2 - (A + C) \lambda + (AC - B^2).
$$
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\arabic*)]
    \item Если $AC - B^2 > 0$, корни одного знака, тип эллиптический.
    \item Если $AC - B^2 < 0$, корни разных знаков, тип гиперболический.
    \item Если $AC - B^2 = 0$, есть нулевой корень, тип параболический.
\end{enumerate}
Случай с~двумя нулевыми собственными числами не~рассматриваем.

\medskip
В~области, где~уравнение~(\ref{eq1:lde2partial-2}) сохраняет свой тип,
можно найти преобразование координат приводящее это уравнение к~каноническому виду во~всей области.
Рассмотрим так называемый метод характеристик.
Решим уравнение характеристик для~простого уравнения второго порядка~(\ref{eq1:lde2partial-2})
$$
A \omega_x^2 + 2 B \omega_x \omega_y + C \omega_y^2 = 0
\quad \Rightarrow \quad
A \left( \frac{\omega_x}{\omega_y} \right)^2 + 2 B \left( \frac{\omega_x}{\omega_y} \right) + C = 0.
$$
Здесь полагаем, что~$\omega_y \neq 0$ (иначе $x \leftrightarrow y$, одновременно быть нулями не~могут, т.к.~вырожденный случай).
И~так как~$\omega(x,y(x)) = \text{const}$, то~благодаря теореме о~неявном отображении,
продифференцировав $\omega(x,y(x))$ по~$x$, получим $y' = -\omega_x / \omega_y$.
Тогда
$$
A (y')^2 - 2 B y' + C = 0 \qquad \text{и} \qquad \frac{D}{4} = B^2 - AC.
$$
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\Roman*.]
    \item Если $B^2 - AC > 0$~--- гиперболический тип, два различных вещественных решения.
    \item Если $B^2 - AC < 0$~--- эллиптический тип, комплексно сопряженные решения.
    \item Если $B^2 - AC = 0$~--- параболический тип, одно решение.
\end{enumerate}
Решения уравнения записывается в~виде $\omega(x,y) = \text{const}$.
Сделаем некоторую невырожденную замену переменных $\xi = \xi(x,y)$, $\eta = \eta(x,y)$.
Пересчитаем производные второго порядка:
\begin{equation*}
\begin{alignedat}{1}
    & u_x = \xi_x \, u_{\xi} + \eta_x \, u_{\eta} \\
    & u_y = \xi_y \, u_{\xi} + \eta_y \, u_{\eta} \\
\end{alignedat}
\qquad \Rightarrow \qquad
\begin{alignedat}{5}
    u_{xx} &=& \,     \xi_x^2 \, u_{\xi \xi}\, &+& \,             2 \, \xi_x \eta_x \, u_{\xi \eta}\, &+& \,      \eta_x^2 & \, u_{\eta \eta} + ... \\
    u_{xy} &=& \, \xi_x \xi_y \, u_{\xi \xi}\, &+& \, (\xi_x \eta_y + \xi_y \eta_x) \, u_{\xi \eta}\, &+& \, \eta_x \eta_y & \, u_{\eta \eta} + ... \\
    u_{yy} &=& \,     \xi_y^2 \, u_{\xi \xi}\, &+& \,             2 \, \xi_y \eta_y \, u_{\xi \eta}\, &+& \,      \eta_y^2 & \, u_{\eta \eta} + ... \\
\end{alignedat}
\end{equation*}
Младшие члены (производные $u$ первого порядка) не~считали, так как на~тип влияют лишь старшие,
которые в~данном случае и~интересны. Подставив производные в исходное уравнение~(\ref{eq1:lde2partial-2}):
\begin{equation*}
\begin{split}
(\overbrace{A  \xi_x^2 + 2 B  \xi_x  \xi_y + C  \xi_y^2)}^{M_1} \, u_{ \xi  \xi} & +
(\overbrace{A \eta_x^2 + 2 B \eta_x \eta_y + C \eta_y^2)}^{M_2} \, u_{\eta \eta}   + \\ & + 2 \, (\underbrace{A \xi_x \eta_x + B \xi_x \eta_y + B \xi_y \eta_x + C \xi_y \eta_y}_{M_3}) u_{\xi \eta} + ... = 0.
\end{split}
\end{equation*}
Заметим, что $M_1$ и~$M_2$~--- левые части уравнения характеристик.

\begin{enumerate}[noitemsep,leftmargin=*,label=\Roman*.]
    \item Гиперболический тип (два вещественных решения).
    В~этом случае делается замена переменных $\xi = \omega_1(x,y)$ и~$\eta = \omega_2(x,y)$.
    В~таком случае, $M_1$ и~$M_2$ зануляются (так~как $\xi$ и~$\eta$~--- решения уравнений характеристик) и~уравнение приводится к~виду
    $$
    u_{\xi \eta} + ... = 0.
    $$
    \item Эллиптический тип (комплексно сопряженные решения).
    В~этом случается делается замена переменных $\xi = \Re \omega$ и~$\eta = \Im \omega$ (знак любой).
    Так~как~$\omega$~--- решение уравнения характеристик, то
    $$
    A (\xi_x \pm i \eta_x)^2 + 2 B (\xi_x \pm i \eta_x) (\xi_y \pm i \eta_y) + C (\xi_y \pm i \eta_y)^2 = 0.
    $$
    Выделим слева вещественную и~мнимую части
    $$
    (A \xi_x^2 + 2 B \xi_x \xi_y + C \xi_y^2) - (A \eta_x^2 + 2 B \eta_x \eta_y + C \eta_y^2) \pm
    2 (A \xi_x \eta_x + B \xi_x \eta_y + B \xi_y \eta_x + C \xi_y \eta_y) \, i = 0 + 0 i.
    $$
    Вещественная часть слева есть ничто иное, как~$M_1 - M_2$,
    и~раз справа вещественная часть тоже ноль, то~получаем, что~$M_1 = M_2$.
    Мнимая часть слева равна $M_3$, то~есть $M_3 = 0$.
    Таким образом уравнение приводится к~виду
    $$
    u_{\xi \xi} + u_{\eta \eta} + ... = 0.
    $$
    \item Параболический тип (одно решение).
    В~этом случается делается замена переменных $\xi = \omega$ и~любая независимая с~$\xi$ новая переменная~$\eta$.
    Так~как $\omega$~--- решение уравнения характеристик, то~$M_1$ зануляется.
    Покажем, что~$M_3 = 0$ для~любого~$\eta$ подходящего условиям выбора.
    Так~как тип параболический, то~$B^2 = AC$. И~допустим, что $\xi_y \neq 0$.
    Если это не~так, то~поменяем $x$ и~$y$ в~выводе.
    $\xi_x$ и~$\xi_y$ одновременно быть нулями не~могут (т.к.~тогда вырожденный случай).
    Вспомним уравнение характеристик и~найдем отношение $\xi_x / \xi_y$.
    $$
    A \xi_x^2 + 2 B \xi_x \xi_y + C \xi_y^2 = 0
    \quad \Rightarrow \quad
    A \left( \frac{\xi_x}{\xi_y} \right)^2 + 2 B \left( \frac{\xi_x}{\xi_y} \right) + C = 0,
    $$
    откуда
    $$
    \frac{\xi_x}{\xi_y} = \frac{-B \pm \sqrt{B^2 - AC}}{A} = -\frac{B}{A}.
    $$
    Теперь в~$M_3$ поделим на~$\xi_y$ и~подставим полученное выше соотношение
    $$
    A \frac{\xi_x}{\xi_y} \eta_x + B \frac{\xi_x}{\xi_y} \eta_y + B \eta_x + C \eta_y = 
    -B \eta_x + - \frac{B^2}{A} \eta_y + B \eta_x + C \eta_y = C \eta_y - C \eta_y = 0.
    $$
    Что и~требовалось.
    Таким образом такой заменой уравнение приводится к~виду
    $$
    u_{\eta \eta} + ... = 0.
    $$ 
\end{enumerate}
Небольшое замечание: так~как замена переменных невырожденная, то~хотя~бы один старший член всегда остается.
Эти три вида уравнений и~есть каноничные виды.

В~завершение рассмотрим простой \textbf{пример}, а~именно~--- уравнение $u_{xx} - y u_{yy} = 0$ при~$y < 0$.
Составим уравнение характеристик и решим~его
$$
\omega_x^2 - y \omega_y^2 = 0
\quad \Rightarrow \quad
(y')^2 = y
\quad \Rightarrow \quad
\frac{dy}{dx} = \pm \sqrt{-y}
\quad \Rightarrow \quad
2 \sqrt{-y} \pm i x = C.
$$
Введем новые переменные и~пересчитаем производные:
\begin{equation*}
\begin{alignedat}{5}
  	& \xi  =           x, & \qquad  \xi_x = 1, & \quad  \xi_y = 0, \\
  	& \eta = 2 \sqrt{-y}, & \qquad \eta_x = 0, & \quad \eta_y = \frac{-1}{\sqrt{-y}}, \\
\end{alignedat}
\qquad \qquad
\begin{alignedat}{4}
  	& u_x = u_{\xi},                        && \qquad u_{xx} = u_{\xi \xi}, \\
  	& u_y = \frac{-1}{\sqrt{-y}} u_{\eta},  && \qquad u_{yy} = \frac{1}{-y} u_{\eta \eta} + u_{\eta} \frac{-1}{2 \sqrt{-y} (-y)}. \\
\end{alignedat}
\end{equation*}
Подставим в~исходное уравнение и~получим каноничный вид
$$
u_{\xi \xi} - y \cdot \frac{1}{-y} u_{\eta \eta} - u_{\eta} \frac{-y}{2 \sqrt{-y} (-y)} = 0
\quad \Rightarrow \quad
u_{\xi \xi} + u_{\eta \eta} - \frac{u_{\eta}}{\eta} = 0.
$$

\subsection{Задача Коши}
Рассмотрим задачу Коши для~данного уравнения
$$
(L u)(x) = \sum\limits_{i,\,j=1}^{n} a_{ij} u_{x_i x_j} + \sum\limits_{k=1}^{n} b_{k} u_{x_k} + c u, \quad u(x_1,...,x_n), 
$$
$$
L u = f, \quad \left. u \right\vert_{S} = \varphi \in C^1, \quad \left. \pdv{u}{l} \right\vert_{S} = \psi \in C, \quad S \in \mathbb{R}^n \text{~--- гладкая поверхность.}
$$
$l$~--- направление, по~которому дифференцируется, но~не~касательная (иначе производная через~$\varphi$ выражается).
Пусть~$S$~--- плоскость $x_n = 0$. Если~$l$~--- касательная, то~$\vec{l} = \{l_1,...,l_{n-1},0\}$, $|\vec{l}| = 1$, $l_i = \cos(l,x_i)$, тогда
$$
\left. \pdv{u}{l} \right\vert_{S} = \left. (\nabla u, \vec{l}) \right\vert_{S} =
\sum\limits_{k=1}^{n-1} \left. u_{x_k} l_k \right\vert_{x_n=0} = 
\sum\limits_{k=1}^{n-1} \left( \left. u \right\vert_{x_n=0} \right)_{x_k} l_k =
\sum\limits_{k=1}^{n-1} \varphi_{x_k} l_k = \psi
\quad \Rightarrow \quad
\psi = F(\varphi).
$$
Также отсюда следует, что~раз касательная компонента выражается через~$\varphi$,
то~достаточно задавать производную по~нормали, так~обычно и~делают: $\pdv{u}{\nu}$, $\nu$~--- нормаль.

Пусть~$S$ задается уравнением $\omega(x) = 0$ и~$\omega \in C^2$.
Рассмотрим так~называемое распрямление поверхности в~окрестности точки~$x^*$.
Также потребуем, чтобы $\nabla \omega \neq 0$.
Не~умаляя общности будем считать, что~$\omega_{x_n}(x^*) \neq 0$.
Сделаем замену $y_k = x_k \, \forall \ k = 1,...,n-1$, $y_n=\omega(x)$. 
$$
J =
\begin{bmatrix} 
               1 &      0 &      0 & \dots  & 0 \\
               0 &      1 &      0 & \dots  & 0 \\
               0 &      0 &      1 & \dots  & 0 \\
          \vdots & \vdots & \vdots & \ddots & 0 \\
    \omega_{x_1} &  \dots & \dots  &  \dots & \omega_{x_n} \\
\end{bmatrix},
\quad \det J = \omega_{x_n} \neq 0.
$$
В~новых переменных $S$ переходит в~$y_n = 0$, $u \rightarrow \widetilde{u}$, $l \rightarrow \widetilde{l}$.
$$
\pdv{u}{x_k} = \sum\limits_{j=1}^{n} \pdv{\widetilde{u}}{y_j} \pdv{y_j}{x_k}
\quad \Rightarrow \quad
\nabla_x u = J^T \nabla_y \widetilde{u}
\quad \Rightarrow \quad
\pdv{u}{l} = (\nabla_x u, l) = (J^T \nabla_y \widetilde{u}, l) = (\nabla_y \widetilde{u}, \underbrace{J l}_{\widetilde{l}}).
$$

Рассмотрим связь данных Коши на~характеристической поверхности.
Пусть выполнено распрямление поверхности~($y_n = 0$). Тогда
$$
\left.\widetilde{u} \right\vert_{y_n=0} = \widetilde{\varphi}, \qquad
\left.\pdv{\widetilde{u}}{y_k} \right\vert_{y_n=0}      = \widetilde{\varphi}_{y_k}, \ k = 1,...,n-1, \qquad
\left.\pdv{\widetilde{u}}{y_k}{y_l} \right\vert_{y_n=0} = \widetilde{\varphi}_{y_k y_l}, \ k, \, l = 1,...,n-1.
$$
$$
\left.\pdv{\widetilde{u}}{y_n}      \right\vert_{y_n=0} = \widetilde{\psi}, \quad
\left.\pdv{\widetilde{u}}{y_n}{y_k} \right\vert_{y_n=0} = \widetilde{\psi}_{y_k}, \ k = 1,...,n-1.
$$
И~тогда уравнение запишется как
$$
L \left.\widetilde{u}\right\vert_{y_n = 0} = \left.\widetilde{a}_{nn} \widetilde{u}_{y_n y_n} \right\vert_{y_n = 0} + 
F(\widetilde{\varphi}, \widetilde{\psi}, \widetilde{a}_{kl}, \widetilde{b}_k, \widetilde{c}, \widetilde{\varphi}_{y_k y_l},...) =
\left.\widetilde{f}\right\vert_{y_n = 0}.
$$
Если окажется, что $\widetilde{a}_{nn} = 0$, то~получится уравнение, которое связывает
$\left.\widetilde{\varphi}, \ \widetilde{\psi}, \ \widetilde{f} \right\vert_{y_n = 0}$.
И~оно может быть переопределено, иначе говоря~--- не~согласовано.
Хорошо~бы ставить задачи~там, где~$\widetilde{a}_{nn} \neq 0$.

Назовем поверхность $S$~характеристической для~оператора $L$~в~точке $x^*$, если
$$
\sum\limits_{k, \, j=1}^{n} a_{kj}(x^*) \nu_k \nu_j = 0,
$$
где~$\nu$~--- нормально к~поверхности~$S$ в~точке~$x^*$.
Если~$S$: $\omega(x) = 0$, $\nu \parallel \nabla \left. \omega \right\vert_{x=x^*}$, то
$$
\sum\limits_{k, \, j = 1}^{n} a_{kj}(x^*) \omega_{x_k} \omega_{x_j} = 0 \qquad \text{(уравнение характеристик).}
$$
Если поверхность характеристическая для~оператора $L$~в~точке~$x^*$,
то~для~существования $C^2$~гладкого решения задачи Коши необходимо выполнение
условия разрешимости уравнения $F(...) = \left.\widetilde{f} \right\vert_{y_n=0}$.
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\arabic*)]
    \item Поверхность~$S$~--- характеристическая, нет~согласования~--- решений~нет.
    \item Поверхность~$S$~--- характеристическая, данные согласованы~--- множество решений.
    \item Поверхность~$S$~--- не~характеристическая, то~теорема Коши-Ковалевской.
\end{enumerate}
\begin{theorem*}[Теорема Коши-Ковалевской]
Если коэффициенты уравнения, $f$, $\varphi$ и~$\psi$ вещественно аналитические (раскладываются в~степенной ряд),
то~решение задачи Коши существует и~единственно.
\end{theorem*}

% добавить подробностей и наглядности важности
\noindent\textbf{Пример:} уравнение теплопроводности.\\
$u(t,x)$, $u_t - u_{xx} = 0$.
Уравнение характеристик: $-\omega_x^2 = 0$.
Прямая $t=0$~--- характеристическая поверхность.
Задается только условие $\left. u \right\vert_{t=0} = \varphi(x)$.

\subsection{Корректность задачи Коши. Пример Адамара}
Корректность задачи:
$$
L u = f, \quad \left. u \right\vert_{S} = \varphi, \quad  \left. \pdv{u}{\nu} \right\vert_{S} = \psi, \quad \mathscr{L}: u \rightarrow F, \quad u \in B_1, \ F \in B_2.
$$
$B_1$, $B_2$~--- банаховы пространства\footnote{%
Банахово пространство~--- полное нормированное векторное пространство (полное по~метрике, порожденной нормой).
Нормированное пространство~--- пространство с~нормой.
Норма~--- функционал на~векторном пространстве (удовлетворяющие определенным аксиомам), обобщение понятия длины вектора.
Метрика~--- функция на~парах элементов множества, вводящая на~нём расстояние (удовлетворяющие определенным аксиомам).
Полное метрическое пространство~--- пространство,
в~котором каждая фундаментальная последовательность (сходящаяся в~себе) сходится к~элементу этого же~пространства.
}.
Если
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\arabic*)]
    \item решение существует при $F \in B_2$,
    \item решение единственно,
    \item непрерывная зависимость решения от~данных задачи, % уточнить: то~есть существует обратный непрерывный оператор,
\end{enumerate}
то~тогда задача поставлена корректно. Рассмотрим пример некорректной постановки, он~же пример Адамара. Поставим задачу
$$
\Delta u = u_{xx} + u_{yy} = 0, \quad  \left. u \right\vert_{y=0} = \varphi(x), \quad  \left. u_y \right\vert_{y=0} = 0.
$$
Рассмотрим начальные данные и~их~асимптотику
$$
\varphi_k = \frac{1}{k} \sin(kx), \quad
\norm{\varphi} = \sup\limits_{\mathbb{R}} |\varphi(x)|, \quad
\norm{\varphi_k} = \frac{1}{k} \xrightarrow[k \to \infty]{} 0.
$$
Несложно убедиться, что
$$
u_k(x,y) = \frac{1}{k} \sin(kx) \ch(ky)
$$
удовлетворяет уравнению. Пусть~$x \in \mathbb{R}$, $y \in [0,h]$. Тогда
$$
\norm{u_k} = \sup\limits_{\substack{x \in \mathbb{R} \\ y \in [0,h]}} \left| \frac{1}{k} \sin(kx) \ch(ky) \right| =
\frac{1}{k} \ch (kh) \xrightarrow[k \to \infty]{} \infty.
$$
Непрерывной зависимости решения от~данных задачи нет.
Для~уравнения Лапласа (и~уравнений эллиптического типа) задача Коши некорректна.
\end{document}
